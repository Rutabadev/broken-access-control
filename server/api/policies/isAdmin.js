module.exports = async function (req, res, proceed) {

  if (sails.config.custom.isBroken) {
    return proceed();
  }

  if (req.session.authenticated) {
    if (req.session.role === 'admin')
      return proceed();
  }

  return res.forbidden();

};
