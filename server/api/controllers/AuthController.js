/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  login: function(req, res) {
    var username = req.body.username;
    var password = req.body.password;

    if (username === "user" && password === "pwd") {
      req.session.authenticated = true;
      req.session.role = 'peon'
      let resObject = {
        status: 200,
        isAdmin: false,
        message: "Bonjour."
      }
      res.json(resObject);
    } else if (username === "admin" && password === "admin") {
      req.session.authenticated = true;
      req.session.role = 'admin'
      let resObject = {
        status: 200,
        isAdmin: true,
        message: "Bonjour admin."
      }
      res.json(resObject);
    } else {
      let resObject = {
        status: 403,
        message: "Clairement tu n'as pas le droit d'être ici, ouste."
      }
      res.status(403).json(resObject);
    }
  },

  logout: function (req, res) {
    req.session.authenticated = false;
    req.session.role = null;
    let resObject = {
      status: 200,
      message: "Au revoir."
    }
    res.json(resObject);
  },

  me: function (req, res) {
    if (req.session.authenticated) {
      let resObject = {
        status: 200,
        isAuthenticated : req.session.authenticated,
        isAdmin: req.session.role === 'admin'
      }
      res.json(resObject)
    } else {
      let resObject = {
        status: 403,
        message: "Clairement tu n'as pas le droit d'être ici, ouste."
      }
      res.status(403).json(resObject)
    }
  }

};

