/**
 * AdminDog.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {type: 'string', required: true},
    race: {type: 'string'},
    age: {type: 'number'},
    master: {type: 'string'},
    isGood: {type: 'boolean', defaultsTo: true},
  },

};

