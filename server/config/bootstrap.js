/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return;
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```

  await Dog.createEach([
    {
      name: "Rookie",
      race: "Fox",
      isGood: false,
      master: "Aya Nakamura",
      age: 2
    },
    {
      name: "Rox",
      race: "Boxer",
      age: 7
    },
    {
      name: "Snoopy",
      race: "Je sais pas trop",
      master: "",
      age: 43
    },
    {
      name: "Milou",
      race: "Merdouille blanche",
      age: 7
    },
    {
      name: "Rantanplan",
      race: "Berger allemand je dirais",
      master: "Lucky Luke vite fait",
      age: 10
    },
    {
      name: "Idéfix",
      race: "Tout petit dalmatien",
      master: "Obélix",
      age: 4
    }
  ])

  await AdminDog.createEach([
    {
      name: "Youki",
      race: "Cavalier King Charles",
      master: "Pompidou",
      age: 8
    },
    {
      name: "Kiki",
      race: "Pug",
      master: "Obama",
      age: 18
    },
    {
      name: "Fabrice Luchini",
      race: "Humain",
      master: "Kim Jong Un",
      age: 67
    },
    {
      name: "Decapitator",
      race: "Chiwawa",
      master: "François Hollande",
      age: 5
    },
  ])

};
