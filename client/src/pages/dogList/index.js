import React, {Component} from 'react'
import './style.css'

export class DogList extends Component {
    state = {
        dogs: []
    }

    async componentDidMount() {
        let req, res
        try {
            req = await fetch(this.props.isAdmin ? 'http://localhost:1337/admindog' : 'http://localhost:1337/dog', {
                credentials: "include",                
            })
            res = await req.json()            
        }
        catch (e) {
            console.log(e)
        }

        if (req.status === 200) {
            this.setState({
                dogs: res
            })
        }
        
    }

    async componentDidUpdate(prevProps, prevState) {
        if (prevProps.isAdmin !== this.props.isAdmin) {
            let req, res
            try {
                req = await fetch(this.props.isAdmin ? 'http://localhost:1337/admindog' : 'http://localhost:1337/dog', {
                    credentials: "include",
                })
                res = await req.json()
            }
            catch (e) {
                console.log(e)
            }

            if (req.status === 200) {
                this.setState({
                    dogs: res
                })
            }
        }
    }

    render(){
        return (
            <div className='dog-list page'>
            {this.state.dogs.map((dog, index) => (
                <div className='dog' key={index}>
                        <div className='dog-icon' style={{ backgroundImage: 'url(/assets/images/pichu.png)'}}>
                    </div>
                    <div className='dog-field'><b>Nom: </b>{dog.name}</div>
                    <div className='dog-field'><b>Race: </b>{dog.race}</div>
                    <div className='dog-field'><b>Age: </b>{dog.age}</div>
                    <div className='dog-field'><b>Dresseur: </b>{dog.master}</div>
                    <div className='dog-field'><b>Gentil: </b>{dog.isGood.toString()}</div>
                </div>
            ))}
            </div>
        )
    }
}