import React, { Component } from 'react';
import './App.css';

import { HomePage } from './pages/homePage'
import { DogList } from './pages/dogList'
import { Header } from './components/header'
import { Footer } from './components/footer'
import { Switch, Route, Redirect } from 'react-router-dom'

class App extends Component {
  state = {
    isLoggedIn: false,
    isAdmin: false
  }

  async componentDidMount() {
    let req = await fetch('http://localhost:1337/me', {
      credentials: "include",      
    })
    let res = await req.json()

    if (req.status === 200) {
      this.setState({
        isLoggedIn: true,
        isAdmin: res.isAdmin
      })
    }
  }

  async login(username, password) {
    let res
    let req = await fetch('http://localhost:1337/login', {
      credentials: "include",
      method: 'post',
      body: JSON.stringify({
        username,
        password
      })
    })
    res = await req.json()

    if(req.status === 200) {
      this.setState({
        isLoggedIn: true,
        isAdmin: res.isAdmin
      })
    } 
  }

  async logout() {
    let req = await fetch('http://localhost:1337/logout', {
      credentials: "include",
      method: 'post'
    })
    await req.json()

    if (req.status === 200) {
      this.setState({
        isLoggedIn: false,
        isAdmin: false
      })
    }
  }

  render() {
    return (
      <div className="App">
        <Header isAdmin={this.state.isAdmin} isLoggedIn={this.state.isLoggedIn} logout={() => this.logout()} />
        <Switch>
          {/* {this.state.isLoggedIn && <Route path='/chiens' component={DogList} />}
          {this.state.isLoggedIn && this.state.isAdmin && <Route path='/admin/chiens' render={props => <DogList {...props} isAdmin={this.state.isAdmin} />} />}
          {!this.state.isLoggedIn && <Route exact path="/" render={props => <HomePage {...props} login={(username, password) => this.login(username, password)} />} />}
          {this.state.isLoggedIn && this.state.isAdmin && <Redirect to="/admin/chiens" />}
          {this.state.isLoggedIn && !this.state.isAdmin && <Redirect to="/chiens" />}
          {!this.state.isLoggedIn && <Redirect to="/" />} */}
          {this.state.isLoggedIn && <Route exact path='/chiens' component={DogList} />}
          {this.state.isLoggedIn && <Route exact path='/admin/chiens' render={props => <DogList {...props} isAdmin={true} />} />}
          {!this.state.isLoggedIn && <Route exact path="/" render={props => <HomePage {...props} login={(username, password) => this.login(username, password)} />} />}
          {this.state.isLoggedIn && <Route exact path="/" component={DogList} />}
        </Switch>
        <Footer/>
      </div>
    );
  }
}

export default App;
