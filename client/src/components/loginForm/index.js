import React, {Component} from 'react'

import './style.css'

class LoginForm extends Component {
    state = {
        username: '',
        password: ''
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.login(this.state.username, this.state.password)
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    }

    render(){
        return (
            <form onSubmit={this.handleSubmit}>
                <div className='title'>
                    connexion
                </div>
                <div className='form-field'>
                    <div className='field-name'>
                        USERNAME :
                    </div>
                    <input type='text' name="username" value={this.state.username} onChange={this.handleChange} />
                </div>

                <div className='form-field'>
                    <div className='field-name'>
                        PASSWORD :
                    </div>
                    <input type='password' name="password" value={this.state.password} onChange={this.handleChange} />
                </div>

                <div className='button'>
                    <input type='submit' value='LOGIN'/>
                </div>
            </form>
        )
    }
}

export { LoginForm };