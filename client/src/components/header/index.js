import React, { Component } from "react";
import { ReactComponent as Psyko } from "./psycho.svg";
import { ReactComponent as Ronflex } from "./ronflex.svg";
import { ReactComponent as Metamorph } from "./metamorph.svg";
import { Link } from 'react-router-dom'
import "./style.css";

class Header extends Component {
  render() {
    let icon;
    let proba = Math.random();
    if (proba < 0.33) {
      icon = <Psyko className="ouf" />;
    } else if (proba > 0.66) {
      icon = <Ronflex className="ouf" />;
    } else {
      icon = <Metamorph className="ouf" />;
    }
    return (
      <header>
        <div className="title">
          Broken access control
          {icon}
        </div>
        <nav>

          {this.props.isLoggedIn && this.props.isAdmin && <div className="nav-item"><Link to="/admin/chiens">Chiens de l'administrateur</Link></div>}
          {this.props.isLoggedIn && <div className="nav-item"><Link to="/chiens">Chiens</Link></div>}
          {this.props.isLoggedIn && <div className="nav-item" onClick={this.props.logout}>Se déconnecter</div>}
        </nav>
      </header>
    );
  }
}

export { Header };
