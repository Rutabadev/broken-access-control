import React, {Component} from 'react'

import './style.css'

class Footer extends Component {

    render(){
        return (
            <footer>
                <div className='copyright'>
                    Copyright &copy;
                </div>
                    <div className='member'>
                        <img src='/assets/images/pichu.png' alt='member1-icon' className='icon'/>
                        <div className='pseudo'>Léo LE POGAM</div>
                    </div>
                    <div className='member'>
                        <img src='/assets/images/psykokwak.png' alt='member2-icon' className='icon' />
                        <div className='pseudo'>Etienne RABY</div>
                    </div>
                    <div className='member'>
                        <img src='/assets/images/kaiminus.png' alt='member3-icon' className='icon' />
                        <div className='pseudo'>Tanguy HELLER</div>
                    </div>
            </footer>
        )
    }
}

export {Footer};